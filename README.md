
# Coyote Vault

## Overview
Coyote Vault is a simple, small, portable secrets manager application with strong encryption. It allows you to store user names, passwords, URLs and free-form notes in an encrypted file protected by one master password.

Features:
* Strong encryption - AES-256-CBC algorithm (SHA-256 is used as password hash)
* Portable - single jar file which can be carried on a USB stick
* Built-in random password generator
* Organize all your username, password, URL, and notes information in one file
* Data import/export in JSON format

## Usage
Java 8 or later is recommended to run Vault. Most platforms have a mechanism to execute `.jar` files (e.g. double-click the `coyotevault-1.0.0.jar`).
You can also run the application from the command line by typing (the password file is optional):

    java -jar coyotevault-1.0.0.jar [password_file]

## How to compile
* Maven: `mvn clean package`
* Gradle: `gradle clean build`

## Configuration
Default configurations can be overridden in `vault.properties` file:

| Configuration key                  | Value type | Default value |
| ---------------------------------- | ---------- | ------------- |
| system.look.and.feel.enabled       | boolean    | true          |
| clear.clipboard.on.exit.enabled    | boolean    | false         |
| default.password.generation.length | integer    | 14            |
| fetch.favicons.enabled             | boolean    | false         |

## Launch4j
Make sure you have a compiled JAR file and place it in the `.\launch4j\lib` directory. This is the artifact being wrapped.

Install the latest Launch4j compiler and open the `.\launch4j\coyotevault.xml` file.  Change the configuration if needed and press the gear icon to generate a wrapper.  Copy the generated EXE to wherever you want.

The EXE contains the jar and wraps it with execution instructions. Place it anywhere and make sure it's on your path.
