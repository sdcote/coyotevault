package coyote.json.data;

public class MyLocaleBean {
    private final String id = "beanId";
    private final String i = "beanI";
    public String getId() {
        return id;
    }
    public String getI() {
        return i;
    }
}
