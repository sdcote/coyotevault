package coyote.json.data;

/**
 * Class that holds our MyNumber override as a property.
 */
public class MyNumberContainer {
  private MyNumber myNumber = new MyNumber();

  /**
   * @return a MyNumber.
   */
  public Number getMyNumber() {
    return this.myNumber;
  }
}
