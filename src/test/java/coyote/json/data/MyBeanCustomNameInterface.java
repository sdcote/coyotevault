package coyote.json.data;

import coyote.json.JSONPropertyIgnore;
import coyote.json.JSONPropertyName;

public interface MyBeanCustomNameInterface {
  @JSONPropertyName("InterfaceField")
  float getSomeFloat();

  @JSONPropertyIgnore
  int getIgnoredInt();
}