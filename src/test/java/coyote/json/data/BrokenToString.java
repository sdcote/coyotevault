package coyote.json.data;

/**
 * test class for verifying write errors.
 */
public class BrokenToString {
  @Override
  public String toString() {
    throw new IllegalStateException("Something went wrong!");
  }
}