package coyote.vault.util;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class UserPreferencesTest {
  public static final String FILENAME = ".deleteMe";

  @BeforeClass
  public static void setupTests() {
    File filePath = new File(System.getProperty("user.home") + File.separator + FILENAME);
    if (filePath.exists() && filePath.isFile()) {
      filePath.delete();
    }
  }

  @Test
  public void constructor() {
    UserPreferences prefs = new UserPreferences(FILENAME, "dontCare");
    assertNotNull(prefs);
  }

  @Test
  public void getNonExistentValue() {
    UserPreferences prefs = new UserPreferences(FILENAME, "nutinHere");
    assertNotNull(prefs);
    String brand = prefs.get("cider.brand");
    assertNull(brand);
  }

  @Test
  public void getDefaultValue() {
    UserPreferences prefs = new UserPreferences(FILENAME, "nutinHereEither");
    assertNotNull(prefs);
    String brand = prefs.get("cider","Downeast");
    assertNotNull(brand);
    assertEquals("Downeast", brand);
  }


  @Test
  public void setValue() {
    UserPreferences prefs = new UserPreferences(FILENAME, "test");
    assertNotNull(prefs);
    prefs.set("cider.brand", "Downeast");
    String brand = prefs.get("cider.brand");
    assertNotNull(brand);
    assertEquals("Downeast", brand);
  }


}