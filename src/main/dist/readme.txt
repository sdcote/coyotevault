# Vault - Password Manager 1.0.0

Overview
--------
Coyote Vault is a simple, small, portable password manager application with
strong encryption. It allows you to store usernames, passwords, URLs and
generic notes in an encrypted file protected by one master password.

Features:
* Strong encryption - AES-256-CBC algorithm
* Portable - single jar file which can be carried on a USB stick
* Built-in random password generator
* Organize all your usernames, passwords, URLs and notes in one file
* Data import/export in JSON format

Usage
-----
Java 8 or later is recommended to run Coyote Vault. Most platforms have a
mechanism to execute `.jar` files (e.g. double click the `coyotevault-1.0.0.jar`).
You can also run the application from the command line by typing (the password
file is optional):

    java -jar coyotevault-1.0.0.jar [password_file]

Configuration
-------------
Default configurations can be overridden in `vault.properties` file:

| Configuration key                  | Value type | Default value |
| ---------------------------------- | ---------- | ------------- |
| system.look.and.feel.enabled       | boolean    | true          |
| clear.clipboard.on.exit.enabled    | boolean    | false         |
| default.password.generation.length | integer    | 14            |
| fetch.favicons.enabled             | boolean    | false         |

