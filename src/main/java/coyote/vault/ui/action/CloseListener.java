package coyote.vault.ui.action;

import coyote.vault.ui.VaultFrame;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Listener for widow close.
 */
public class CloseListener extends WindowAdapter {

  /**
   * Calls the {@code exitFrame} method of main frame.
   *
   * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
   */
  @Override
  public void windowClosing(WindowEvent event) {
    if (event.getSource() instanceof VaultFrame) {
      ((VaultFrame) event.getSource()).exitFrame();
    }
  }

}
