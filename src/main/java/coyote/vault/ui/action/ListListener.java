package coyote.vault.ui.action;

import coyote.vault.ui.helper.EntryHelper;
import coyote.vault.ui.VaultFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Mouse listener for the entry title list.
 */
public class ListListener extends MouseAdapter {

  /**
   * Show entry on double click.
   *
   * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
   */
  @Override
  public void mouseClicked(MouseEvent evt) {
    if (VaultFrame.getInstance().isProcessing()) {
      return;
    }
    if (SwingUtilities.isLeftMouseButton(evt) && evt.getClickCount() == 2) {
      EntryHelper.editEntry(VaultFrame.getInstance());
    }
  }

  /**
   * Handle pop-up.
   *
   * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
   */
  @Override
  public void mousePressed(MouseEvent evt) {
    checkPopup(evt);
  }

  /**
   * Handle pop-up.
   *
   * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
   */
  @Override
  public void mouseReleased(MouseEvent evt) {
    checkPopup(evt);
  }

  /**
   * Checks pop-up trigger.
   *
   * @param evt mouse event
   */
  private void checkPopup(MouseEvent evt) {
    if (VaultFrame.getInstance().isProcessing()) {
      return;
    }
    if (evt.isPopupTrigger()) {
      JList list = VaultFrame.getInstance().getEntryTitleList();
      if (list.isEnabled()) {
        Point point = new Point(evt.getX(), evt.getY());
        list.setSelectedIndex(list.locationToIndex(point));
        VaultFrame.getInstance().getPopup().show(evt.getComponent(), evt.getX(), evt.getY());
      }
    }
  }

}
