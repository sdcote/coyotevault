package coyote.vault.ui;

import coyote.vault.ui.helper.EntryHelper;
import coyote.vault.util.SpringUtilities;
import coyote.vault.util.StringUtil;
import coyote.vault.Entry;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Arrays;

/**
 * A dialog with the entry data.
 */
public class EntryDialog extends JDialog implements ActionListener {

  private static final long serialVersionUID = 175220948895406617L;
  private static final char NULL_ECHO = '\0';

  private final JPanel fieldPanel;
  private final JPanel notesPanel;
  private final JPanel buttonPanel;
  private final JPanel passwordButtonPanel;

  private final JTextField titleField;
  private final JTextField userField;
  private final JTextField emailField;
  private final JTextField tokenField;
  private final JPasswordField passwordField;
  private final JPasswordField repeatField;
  private final JTextField urlField;
  private final JTextArea notesField;

  private final JButton okButton;
  private final JButton cancelButton;
  private final JToggleButton showButton;
  private final JButton generateButton;
  private final JButton copyButton;

  private final char ORIGINAL_ECHO;
  private final boolean newEntry;
  private Entry formData;
  private String originalTitle;

  /**
   * Creates a new EntryDialog instance.
   *
   * @param parent   parent component
   * @param title    dialog title
   * @param entry    the entry
   * @param newEntry new entry marker
   */
  public EntryDialog(final VaultFrame parent, final String title, final Entry entry, final boolean newEntry) {
    super(parent, title, true);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    this.newEntry = newEntry;

    this.formData = null;

    this.fieldPanel = new JPanel();

    this.fieldPanel.add(new JLabel("Name:"));
    this.titleField = TextComponentFactory.newTextField();
    this.fieldPanel.add(this.titleField);

    this.fieldPanel.add(new JLabel("URL:"));
    this.urlField = TextComponentFactory.newTextField();
    this.fieldPanel.add(this.urlField);

    this.fieldPanel.add(new JLabel("eMail:"));
    this.emailField = TextComponentFactory.newTextField();
    this.fieldPanel.add(this.emailField);

    this.fieldPanel.add(new JLabel("Token:"));
    this.tokenField = TextComponentFactory.newTextField();
    this.fieldPanel.add(this.tokenField);

    this.fieldPanel.add(new JLabel("Username:"));
    this.userField = TextComponentFactory.newTextField();
    this.fieldPanel.add(this.userField);

    this.fieldPanel.add(new JLabel("Password:"));
    this.passwordField = TextComponentFactory.newPasswordField(true);
    this.ORIGINAL_ECHO = this.passwordField.getEchoChar();
    this.fieldPanel.add(this.passwordField);

    this.fieldPanel.add(new JLabel("Repeat:"));
    this.repeatField = TextComponentFactory.newPasswordField(true);
    this.fieldPanel.add(this.repeatField);

    this.fieldPanel.add(new JLabel(""));
    this.passwordButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    this.showButton = new JToggleButton("Show", MessageDialog.getIcon("show"));
    this.showButton.setActionCommand("show_button");
    this.showButton.setMnemonic(KeyEvent.VK_S);
    this.showButton.addActionListener(this);
    this.passwordButtonPanel.add(this.showButton);
    this.generateButton = new JButton("Generate", MessageDialog.getIcon("generate"));
    this.generateButton.setActionCommand("generate_button");
    this.generateButton.setMnemonic(KeyEvent.VK_G);
    this.generateButton.addActionListener(this);
    this.passwordButtonPanel.add(this.generateButton);
    this.copyButton = new JButton("Copy", MessageDialog.getIcon("keyring"));
    this.copyButton.setActionCommand("copy_button");
    this.copyButton.setMnemonic(KeyEvent.VK_C);
    this.copyButton.addActionListener(this);
    this.passwordButtonPanel.add(this.copyButton);
    this.fieldPanel.add(this.passwordButtonPanel);

    this.fieldPanel.setLayout(new SpringLayout());
    SpringUtilities.makeCompactGrid(this.fieldPanel, 8, 2, 5, 5, 5, 5);

    this.notesPanel = new JPanel(new BorderLayout(5, 5));
    this.notesPanel.setBorder(new EmptyBorder(0, 5, 0, 5));
    this.notesPanel.add(new JLabel("Notes:"), BorderLayout.NORTH);

    this.notesField = TextComponentFactory.newTextArea();
    this.notesField.setFont(TextComponentFactory.newTextField().getFont());
    this.notesField.setLineWrap(true);
    this.notesField.setWrapStyleWord(true);
    this.notesPanel.add(new JScrollPane(this.notesField), BorderLayout.CENTER);

    this.buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

    this.okButton = new JButton("OK", MessageDialog.getIcon("accept"));
    this.okButton.setActionCommand("ok_button");
    this.okButton.setMnemonic(KeyEvent.VK_O);
    this.okButton.addActionListener(this);
    this.buttonPanel.add(this.okButton);

    this.cancelButton = new JButton("Cancel", MessageDialog.getIcon("cancel"));
    this.cancelButton.setActionCommand("cancel_button");
    this.cancelButton.setMnemonic(KeyEvent.VK_C);
    this.cancelButton.addActionListener(this);
    this.buttonPanel.add(this.cancelButton);

    getContentPane().add(this.fieldPanel, BorderLayout.NORTH);
    getContentPane().add(this.notesPanel, BorderLayout.CENTER);
    getContentPane().add(this.buttonPanel, BorderLayout.SOUTH);

    fillDialogData(entry);
    setSize(420, 420);
    setMinimumSize(new Dimension(370, 300));
    setLocationRelativeTo(parent);
    setVisible(true);
  }

  /**
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    String command = e.getActionCommand();
    if ("show_button".equals(command)) {
      this.passwordField.setEchoChar(this.showButton.isSelected() ? NULL_ECHO : this.ORIGINAL_ECHO);
      this.repeatField.setEchoChar(this.showButton.isSelected() ? NULL_ECHO : this.ORIGINAL_ECHO);
    } else if ("ok_button".equals(command)) {
      if (this.titleField.getText().trim().isEmpty()) {
        MessageDialog.showWarningMessage(this, "Please fill the title field.");
        return;
      } else if (!checkEntryTitle()) {
        MessageDialog.showWarningMessage(this, "Title is already exists,\nplease enter a different title.");
        return;
      } else if (!Arrays.equals(this.passwordField.getPassword(), this.repeatField.getPassword())) {
        MessageDialog.showWarningMessage(this, "Password and repeated password are not identical.");
        return;
      }
      setFormData(fetchDialogData());
      dispose();
    } else if ("cancel_button".equals(command)) {
      dispose();
    } else if ("generate_button".equals(command)) {
      GeneratePasswordDialog gpd = new GeneratePasswordDialog(this);
      String generatedPassword = gpd.getGeneratedPassword();
      if (generatedPassword != null && !generatedPassword.isEmpty()) {
        this.passwordField.setText(generatedPassword);
        this.repeatField.setText(generatedPassword);
      }
    } else if ("copy_button".equals(command)) {
      EntryHelper.copyEntryField(VaultFrame.getInstance(), String.valueOf(this.passwordField.getPassword()));
    }
  }

  /**
   * Fills the form with the data of given entry.
   *
   * @param entry an entry
   */
  private void fillDialogData(Entry entry) {
    if (entry == null) {
      return;
    }
    this.originalTitle = entry.getName() == null ? "" : entry.getName();
    this.titleField.setText(this.originalTitle + (this.newEntry ? " (copy)" : ""));
    this.userField.setText(entry.getUsername() == null ? "" : entry.getUsername());
    this.passwordField.setText(entry.getPassword() == null ? "" : entry.getPassword());
    this.repeatField.setText(entry.getPassword() == null ? "" : entry.getPassword());
    this.urlField.setText(entry.getUrl() == null ? "" : entry.getUrl());
    this.emailField.setText(entry.getEmail() == null ? "" : entry.getEmail());
    this.tokenField.setText(entry.getToken() == null ? "" : entry.getToken());
    this.notesField.setText(entry.getNotes() == null ? "" : entry.getNotes());
    this.notesField.setCaretPosition(0);
  }

  /**
   * Retrieves the form data.
   *
   * @return an entry
   */
  private Entry fetchDialogData() {
    Entry entry = new Entry();

    String title = StringUtil.handleTroublesomeJsonCharacters(this.titleField.getText());
    String user = StringUtil.handleTroublesomeJsonCharacters(this.userField.getText());
    String password = StringUtil.handleTroublesomeJsonCharacters(String.valueOf(this.passwordField.getPassword()));
    String url = StringUtil.handleTroublesomeJsonCharacters(this.urlField.getText());
    String email = StringUtil.handleTroublesomeJsonCharacters(this.emailField.getText());
    String token = StringUtil.handleTroublesomeJsonCharacters(this.tokenField.getText());
    String notes = StringUtil.handleTroublesomeJsonCharacters(this.notesField.getText());

    entry.setName(title == null || title.isEmpty() ? null : title);
    entry.setUsername(user == null || user.isEmpty() ? null : user);
    entry.setPassword(password == null || password.isEmpty() ? null : password);
    entry.setUrl(url == null || url.isEmpty() ? null : url);
    entry.setEmail(email == null || email.isEmpty() ? null : email);
    entry.setToken(token == null || token.isEmpty() ? null : token);
    entry.setNotes(notes == null || notes.isEmpty() ? null : notes);

    return entry;
  }

  /**
   * Gets the form data (entry) of this dialog.
   *
   * @return nonempty form data if the 'OK1 button is pressed, otherwise an empty data
   */
  public Entry getFormData() {
    return this.formData;
  }

  /**
   * Sets the form data.
   *
   * @param formData form data
   */
  private void setFormData(Entry formData) {
    this.formData = formData;
  }

  /**
   * Checks the entry title.
   *
   * @return if the entry title is already exists in the data model than returns {@code false},
   * otherwise {@code true}
   */
  private boolean checkEntryTitle() {
    boolean titleIsOk = true;
    VaultFrame parent = VaultFrame.getInstance();
    String currentTitleText = StringUtil.handleTroublesomeJsonCharacters(this.titleField.getText());
    if (currentTitleText == null) {
      currentTitleText = "";
    }
    if (this.newEntry || !currentTitleText.equalsIgnoreCase(this.originalTitle)) {
      for (Entry entry : parent.getModel().getEntries().getEntry()) {
        if (currentTitleText.equalsIgnoreCase(entry.getName())) {
          titleIsOk = false;
          break;
        }
      }
    }
    return titleIsOk;
  }
}
