package coyote.vault.util;

import coyote.json.JSONArray;
import coyote.json.JSONObject;
import coyote.vault.Entries;
import coyote.vault.Entry;
import coyote.vault.Vault;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class JsonMarshaller {

  /**
   * Read bytes from the input stream and marshal them onto entries.
   *
   * <p>Data is assumed to be a UTF-8 string representing valid JSON.</p>
   *
   * @param inputStream
   * @return
   * @throws IOException
   */
  public Entries read(InputStream inputStream) throws IOException {
    Entries retval = new Entries();

    byte[] bytes = readBytes(inputStream);
    JSONObject json = new JSONObject(new String(bytes, StandardCharsets.UTF_8));
    JSONArray array = json.getJSONArray(Vault.ENTRIES_TAG);

    for (int i = 0; i < array.length(); i++) {
      JSONObject item = array.getJSONObject(i);
      if (item != null) {
        Entry entry = new Entry();
        if (item.has(Vault.NAME_TAG)) entry.setName(item.getString(Vault.NAME_TAG));
        if (item.has(Vault.NOTES_TAG)) entry.setNotes(item.getString(Vault.NOTES_TAG));
        if (item.has(Vault.PASSWORD_TAG)) entry.setPassword(item.getString(Vault.PASSWORD_TAG));
        if (item.has(Vault.PRIVATE_KEY_TAG)) entry.setPrivateKey(item.getString(Vault.PRIVATE_KEY_TAG));
        if (item.has(Vault.PUBLIC_KEY_TAG)) entry.setPublicKey(item.getString(Vault.PUBLIC_KEY_TAG));
        if (item.has(Vault.URL_TAG)) entry.setUrl(item.getString(Vault.URL_TAG));
        if (item.has(Vault.EMAIL_TAG)) entry.setEmail(item.getString(Vault.EMAIL_TAG));
        if (item.has(Vault.TOKEN_TAG)) entry.setToken(item.getString(Vault.TOKEN_TAG));
        if (item.has(Vault.USER_TAG)) entry.setUsername(item.getString(Vault.USER_TAG));
        retval.getEntry().add(entry);
      }
    }
    return retval;
  }

  /**
   * Write the given entries out to the output stream as UTF-8 JSON encoded string.
   *
   * @param entries
   * @param outputStream
   * @throws IOException
   */
  public void write(Entries entries, OutputStream outputStream) throws IOException {
    outputStream.write(new JSONObject(entries).toString().getBytes(StandardCharsets.UTF_8));
  }


  /**
   * Reads the input stream to the end of file.
   *
   * @param inputStream the input stream to read
   * @return the bytes read in from that stream
   * @throws IOException if there were problems reading the given input stream
   */
  private byte[] readBytes(InputStream inputStream) throws IOException {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int len;
    while ((len = inputStream.read(buffer)) != -1) {
      os.write(buffer, 0, len);
    }
    return os.toByteArray();
  }

}
