package coyote.vault.util;

/**
 * String utility class.
 */
public final class StringUtil {

  private StringUtil() {
    // utility class
  }

  /**
   * This method ensures that the output String has only valid JSON string characters.
   *
   * @param in The String whose non-valid characters we want to remove.
   * @return The in String, stripped of non-valid characters.
   */
  public static String handleTroublesomeJsonCharacters(final String in) {
    if (in == null || in.isEmpty()) {
      return in;
    }
    StringBuilder out = new StringBuilder();
    char current;
    for (int i = 0; i < in.length(); i++) {
      current = in.charAt(i);

      // Right now only look for double quotes...maybe more later
      if (current == 0x22) {
        out.append(0x27);
      } else {
        out.append(current);
      }
    }
    return out.toString();
  }

  public static String stripString(String text) {
    return stripString(text, 80);
  }

  public static String stripString(String text, int length) {
    String result = text;
    if (text != null && text.length() > length) {
      result = text.substring(0, length) + "...";
    }
    return result;
  }

  public static String byteArrayToHex(byte[] array) {
    StringBuilder sb = new StringBuilder(array.length * 2);
    for (byte b : array) {
      sb.append(String.format("%02x", b));
    }
    return sb.toString();
  }


  /**
   * Checks if a string is null, empty ("") or only whitespace.
   *
   * @param str the String to check, may be null
   * @return {@code true} if the argument is empty or null or only whitespace
   */
  public static boolean isBlank(String str) {
    int strLen;
    if (str == null || (strLen = str.length()) == 0) {
      return true;
    }
    for (int i = 0; i < strLen; i++) {
      if ((Character.isWhitespace(str.charAt(i)) == false)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if a string is not null, empty ("") and not only whitespace.
   *
   * <p>This is a convenience wrapper around isBlank(String) to make code
   * slightly more readable.</p>
   *
   * @param str the String to check, may be null
   * @return <code>true</code> if the String is not empty and not null and not
   * whitespace
   * @see #isBlank(String)
   */
  public static boolean isNotBlank(String str) {
    return !StringUtil.isBlank(str);
  }

}
